package de.hfu;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;

public class BaseResidentServiceTest {

    private ResidentRepositoryStub createResidentRepositoryStub() {
        List<Resident> residentlist = new ArrayList<>();
        residentlist.add(new Resident("Peter", "Stark", "Gelbstraße", "City1", new Date()));
        residentlist.add(new Resident("Alina", "Wind", "Blaustraße", "City2", new Date()));
        residentlist.add(new Resident("Hans", "Wind", "Rotstraße", "City3", new Date()));
        return new ResidentRepositoryStub(residentlist);
    }

    @Test
    public void testGetFilteredResidentsList() {
        
        ResidentRepository residentRepositoryStub = createResidentRepositoryStub();
        BaseResidentService baseResidentService = new BaseResidentService();

        baseResidentService.setResidentRepository(residentRepositoryStub);

        // Test 1
        Resident firstFilterResident = new Resident("Peter", null, null, null, null);
        List<Resident> firstResult = baseResidentService.getFilteredResidentsList(firstFilterResident);
        assertEquals(1, firstResult.size());

        // Test 2
        Resident secondFilterResident = new Resident(null, "Stark", "Gelbstraße", null, null);
        List<Resident> secondResult = baseResidentService.getFilteredResidentsList(secondFilterResident);
        assertEquals(1, secondResult.size());

        // Test 3
        Resident thirdFilterResident = new Resident(null, null, null, null, null);
        List<Resident> thirdResult = baseResidentService.getFilteredResidentsList(thirdFilterResident);
        assertFalse(thirdResult.size()==0);

        // Test 4
        Resident fourthFilterResident = new Resident(null, "Wind", null, null, null);
        List<Resident> fourthResult = baseResidentService.getFilteredResidentsList(fourthFilterResident);
        assertEquals(2, fourthResult.size());

        // Test 5
        Resident fifthFilterResident = new Resident("Peter", "Stark", "Gelbstraße", "City1", new Date());
        List<Resident> fifthResult = baseResidentService.getFilteredResidentsList(fifthFilterResident);
        assertEquals(1, fifthResult.size());
    }

    @Test
    public void testGetUniqueResident() throws ResidentServiceException {
        
        ResidentRepository residentRepositoryStub = createResidentRepositoryStub();
        BaseResidentService baseResidentService = new BaseResidentService();

        baseResidentService.setResidentRepository(residentRepositoryStub);

        // Test 1
        Resident firstFilterResident = new Resident("Hans","Wind", null, null, null);
        Resident firstResult = baseResidentService.getUniqueResident(firstFilterResident);
        assertEquals("Hans", firstResult.getGivenName());
        assertEquals("Wind", firstResult.getFamilyName());

        // Test 2
        Resident secondFilterResident = new Resident(null, null, "Blaustraße", null, null);
        Resident secondResult = baseResidentService.getUniqueResident(secondFilterResident);
        assertNotSame("Gelbstraße", secondResult.getStreet());
        assertNotSame("Rotstraße", secondResult.getStreet());
        
        // Test 3
        Resident thirdFilterResident = new Resident(null,"Wind", null, null, null);
        assertThrows(ResidentServiceException.class, () -> {baseResidentService.getUniqueResident(thirdFilterResident);}, "ResidentServiceException was expected");

        // Test 4
        Resident fourthFilterResident = new Resident(null, null, null, "City1", null);
        assertThrows(ResidentServiceException.class, () -> {baseResidentService.getUniqueResident(fourthFilterResident);}, "ResidentServiceException was expected");

        // Test 5
        Resident fifthFilterResident = new Resident("*", null, null, null, new Date());
        assertThrows(ResidentServiceException.class, () -> {baseResidentService.getUniqueResident(fifthFilterResident);}, "ResidentServiceException was expected");

        // Test 6
        Resident sixthFilterResident = new Resident("*", null, null, null, new Date());
        assertThrows(ResidentServiceException.class, () -> {baseResidentService.getUniqueResident(sixthFilterResident);}, "ResidentServiceException was expected");
    }

}
