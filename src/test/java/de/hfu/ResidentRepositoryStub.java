package de.hfu;

import java.util.List;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;

public class ResidentRepositoryStub implements ResidentRepository {

    private List<Resident> residents;

    public ResidentRepositoryStub(List<Resident> residents) {
        this.residents = residents;
    }

    @Override
    public List<Resident> getResidents() {
        return residents;
    }
    
}
